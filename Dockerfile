FROM ubuntu:20.04

#File system arg's
ARG DEBIAN_FRONTEND=noninteractive
ARG WRK_DIR=/app
ARG BUILD_DIR=${WRK_DIR}/build
ARG XDG_RUNTIME_DIR=/tmp/runtime-root
#git arg's
ARG GUI_REPO="https://github.com/easymodo/qimgv.git"
ARG BRANCH_TAG=v1.0.2

RUN apt update -y
RUN apt install -y build-essential \
                   git \
                   cmake \
                   qtbase5-dev \
                   libqt5svg5-dev \
                   libexiv2-dev \
                   libmpv-dev \
                   libopencv-dev \
                   kimageformat-plugins \
                   gcc \
                   g++ \
                   xauth

RUN git clone --branch ${BRANCH_TAG} --single-branch ${GUI_REPO} ${WRK_DIR}

RUN mkdir -p ${BUILD_DIR} && cd ${BUILD_DIR} && cmake .. && make -j $(nproc)


CMD ["/bin/bash"]
