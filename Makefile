IMG_NAME=qt-demo
IMG_TAG=latest

.PHONY: build run_debug run
build:
	docker build -t $(IMG_NAME):$(IMG_TAG) .

run_debug:
	docker run -it -v /tmp/.X11-unix/:/tmp/.X11-unix/ --net=host \
			-v ~/Pictures:/home/zack/Pictures -e DISPLAY=${DISPLAY} --rm $(IMG_NAME):$(IMG_TAG)

run:
	docker run $(IMG_NAME):$(IMG_TAG)
